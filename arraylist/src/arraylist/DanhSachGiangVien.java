/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraylist;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Zero
 */
public class DanhSachGiangVien {

    private static class GiangVien {
        public GiangVien() {
        }
    }
    ArrayList<GiangVien> danhSach;
    Scanner sc2 = new Scanner(System.in);

    public DanhSachGiangVien() {
        this.danhSach = new ArrayList<GiangVien>();
    }

    public DanhSachGiangVien(ArrayList<GiangVien> danhSach) {
        this.danhSach = danhSach;
    }

    public void addGiangVien(GiangVien giangvien) {
        this.danhSach.add(giangvien);
    }

    public void hienThiThongSoGiangVien() {
        for (GiangVien item : this.danhSach) {
            System.out.println("Ma Giang vien :" + item.getMaGV() + " ten giang vien : " + item.getHoTen()
                    + " Luong co ban : " + item.getLuongCoBan() + " So gio day: " + item.getSoGioDay()
                    + " Tong Luong : " + this.tinhLuong(item.getLuongCoBan(), item.getSoGioDay())
                    + " Tuoi cua giang vien: " + this.tinhTuoi(item.getNamSinh()));
        }
    }

    public int tinhTuoi(int n) {
        return (int) (2022 - n);
    }

    public int tinhLuong(int item1, int item2) {
        return (int) ((item1 * item2) - (10 * (item1 * item2) / 100));
    }

    public void hienThiSoGiangVien() {
        System.out.println(" Co tat ca  " + this.danhSach.size() + " giang vien ");
    }

    public void lamRongDanhSach() {
        this.danhSach.removeAll(danhSach);
    }

    String layMaGiangVien() {
        String b;
        System.out.println("Nhap vao ma GV muon tim ");
        b = sc2.nextLine();
        return b;
    }

    void XoaGiangVien() {
    }
}
