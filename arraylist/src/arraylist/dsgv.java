/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraylist;

import java.util.ArrayList;

/**
 *
 * @author Zero
 */
public class dsgv {

    private ArrayList<giangvien> danhSach;

    public dsgv() {
        this.danhSach = new ArrayList<giangvien>();
    }

    public dsgv(ArrayList<giangvien> danhSach) {
        this.danhSach = danhSach;
    }

    public void themgv(giangvien gv) {
        this.danhSach.add(gv);
    }

    public void indsgv() {
        for (giangvien sinhVien : danhSach) {
            System.out.println(sinhVien);
        }
    }
}
