/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraylist;

import java.util.ArrayList;

/**
 *
 * @author Zero
 */
public class dssv {
		private ArrayList<sv> danhSach;
		
		public dssv() {
			this.danhSach = new ArrayList<sv>();
		}
		
		public dssv(ArrayList<sv> danhSach) {
			this.danhSach = danhSach;
		}
		
		public void themSinhVien(sv sv) {
			this.danhSach.add(sv);
		}
		
		public void inDanhSachSinhVien() {
			for (sv sinhVien : danhSach) {
				System.out.println(sinhVien);
			}
		}
		
		public boolean checknull() {
			return this.danhSach.isEmpty();
		}
		
		public int getnumbersv() {
			return this.danhSach.size();
		}
		
		public void emptydata() {
			this.danhSach.removeAll(danhSach);
		}
		
		public boolean checkexist(sv sv) {
			return this.danhSach.contains(sv);
		}
		
		public boolean delsv(sv sv) {
			return this.danhSach.remove(sv);
		}
		
		public void findsv(String ten) {
			for (sv sinhVien : danhSach) {
				if(sinhVien.getHoVaTen().indexOf(ten)>=0) {
					System.out.println(sinhVien);
				}
			}
		}

}