/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraylist;

/**
 *
 * @author Zero
 */
public class giangvien {

    String mgv, hoten;
    int namsinh, lcb, sogio;

    public giangvien(String mgv, String hoten, int namsinh, int lcb, int sogio) {
        this.mgv = mgv;
        this.hoten = hoten;
        this.namsinh = namsinh;
        this.sogio = sogio;
        this.lcb = lcb;
    }

    public String getmgv() {
        return mgv;
    }

    public void setmgv(String mgv) {
        this.mgv = mgv;
    }

    public String gethoten() {
        return hoten;
    }

    public void sethoten(String hoten) {
        this.hoten = hoten;
    }

    public int getnamsinh() {
        return namsinh;
    }

    public void setnamsinh(int namsinh) {
        this.namsinh = namsinh;
    }

    public float getsogio() {
        return sogio;
    }

    public void setsogio(int sogio) {
        this.sogio = sogio;
    }

    public float getlcb() {
        return lcb;
    }

    public void setlcb(int lcb) {
        this.lcb = lcb;
    }

    @Override
    public String toString() {
        return "SinhVien [mgv=" + mgv + ", hoten=" + hoten + ", namsinh=" + namsinh + ", lcb=" + lcb + ", sogio" + sogio + "]";
    }

}
